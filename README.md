# needs

Dieses Beispiel-Projekt zeigt wie man mittels des `needs` keywords gerichtete azyklische Abhängigkeitsgraphen für Jobs konstruieren kann. 

In der Beispiel Pipeline melden alle Jobs sofort `success` zurück, bis auf die Jobs mit dem Suffix `b`, die jeweils ein `sleep 30` Kommando enthalten. Im Ablauf der Pipeline lässt sich erkennen, dass die die `a` Jobs unabhängig von den `b` Jobs durchlaufen, ohne auf die jeweilige Stage zu warten.

Leider ist die Unterstützung für die Anzeige der DAG (Directed Acyclic Graph) Dependencies zum Zeitpunkt dieses Commits noch nicht gegeben. GitLab arbeitet daran die Darstellung der Pipelines zu verändern, um die Abhängigkeiten sauber abbilden zu können.

Weitere Informationen zu `needs` und DAG gibt es in der [GitLab-Doku](https://docs.gitlab.com/ee/ci/yaml/#needs).
